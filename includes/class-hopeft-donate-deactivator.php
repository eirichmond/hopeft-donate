<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.squareonemd.co.uk
 * @since      2.0.0
 *
 * @package    Hopeft_Donate
 * @subpackage Hopeft_Donate/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      2.0.0
 * @package    Hopeft_Donate
 * @subpackage Hopeft_Donate/includes
 * @author     Your Name <email@example.com>
 */
class Hopeft_Donate_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    2.0.0
	 */
	public static function deactivate() {

	}

}
