<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://www.squareonemd.co.uk
 * @since      2.0.0
 *
 * @package    Hopeft_Donate
 * @subpackage Hopeft_Donate/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      2.0.0
 * @package    Hopeft_Donate
 * @subpackage Hopeft_Donate/includes
 * @author     Your Name <email@example.com>
 */
class Hopeft_Donate_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    2.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'hopeft-donate',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
