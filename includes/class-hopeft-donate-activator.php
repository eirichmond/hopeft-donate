<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.squareonemd.co.uk
 * @since      2.0.0
 *
 * @package    Hopeft_Donate
 * @subpackage Hopeft_Donate/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      2.0.0
 * @package    Hopeft_Donate
 * @subpackage Hopeft_Donate/includes
 * @author     Your Name <email@example.com>
 */
class Hopeft_Donate_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    2.0.0
	 */
	public static function activate() {
		
		// setup the database structure		
	    global $wpdb;

	    $donations_table = $wpdb->prefix . 'hft_donations';
	    $donations = "CREATE TABLE " . $donations_table . " (
			donation_id int(11) NOT NULL AUTO_INCREMENT,
			donation_type VARCHAR(60) NOT NULL,
			donation_amount BIGINT(20) NOT NULL,
			donor_id BIGINT(20) NOT NULL,
			hft_id LONGTEXT,
			donation_date DATETIME NOT NULL,
			stripe_identifier VARCHAR(60) NOT NULL,
			gc_redirect_identifier VARCHAR(60) NOT NULL,
			gc_subscription_identifier VARCHAR(60) NOT NULL,
			gc_mandate_identifier VARCHAR(60) NOT NULL,
			hear_about_us LONGTEXT,
			hear_about_us_other LONGTEXT,
			personal_preferences LONGTEXT,
			gift_aid LONGTEXT,
			UNIQUE KEY donation_id (donation_id)
	    );";

	    $donors_table = $wpdb->prefix . 'hft_donors';
	    $donors = "CREATE TABLE " . $donors_table . " (
			donor_id int(11) NOT NULL AUTO_INCREMENT,
			donation_id BIGINT(20) NOT NULL,
			donor_name VARCHAR(60) NOT NULL,
			donor_email VARCHAR(60) NOT NULL,
			gc_customer_id VARCHAR(60) NOT NULL,
			billing_address_line_1 VARCHAR(60) NOT NULL,
			billing_zip VARCHAR(9) NOT NULL,
			billing_city VARCHAR(60) NOT NULL,
			billing_country VARCHAR(60) NOT NULL,
			billing_country_code VARCHAR(4) NOT NULL,
			created_date DATETIME NOT NULL,
			hear_about_us LONGTEXT,
			hear_about_us_other LONGTEXT,
			personal_preferences LONGTEXT,
			gift_aid LONGTEXT,
			UNIQUE KEY donor_id (donor_id)
	    );";
	    
	    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	    dbDelta( $donations );
	    dbDelta( $donors );

		// setup some basic pages		
		$sub_donation_pages = array(
			array('slug' => 'donate-details', 'title' => 'Donate Details'),
			array('slug' => 'donate-confirmation', 'title' => 'Donate Confirmation'),
			array('slug' => 'donate-payment', 'title' => 'Donate Payment'),
			array('slug' => 'donate-successful', 'title' => 'Donation Successful'),
		);
		
		// Initialize the page ID to -1. This indicates no action has been taken.
		$users_page = -1;
	
		// Setup the author, slug, and title for the post
		$author_id = 1;
		$slug = 'donate-hope-for-tomorrow';
		$title = 'Donate to Hope for Tomorrow';
	
		// If the page doesn't already exist, then create it
		if( null == get_page_by_title( $title ) ) {
	
			// Set the post ID so that we know the post was created successfully
			$users_page = wp_insert_post(
				array(
					'comment_status'	=>	'closed',
					'ping_status'		=>	'closed',
					'post_author'		=>	$author_id,
					'post_name'		=>	$slug,
					'post_title'		=>	$title,
					'post_status'		=>	'publish',
					'post_type'		=>	'page'
				)
			);
	
		// Otherwise, we'll stop
		} else {
	
	    		// Arbitrarily use -2 to indicate that the page with the title already exists
	    		$users_page = -2;
	
		} // end if
		
		foreach ($sub_donation_pages as $subpage) {

			// Setup the author, slug, and title for the post
			$author_id = 1;
			$slug = $subpage['slug'];
			$title = $subpage['title'];
		
			// If the page doesn't already exist, then create it
			if( null == get_page_by_title( $title ) ) {
		
				// Set the post ID so that we know the post was created successfully
				$post_id = wp_insert_post(
					array(
						'comment_status'	=>	'closed',
						'ping_status'		=>	'closed',
						'post_author'		=>	$author_id,
						'post_name'		=>	$slug,
						'post_title'		=>	$title,
						'post_status'		=>	'publish',
						'post_type'		=>	'page',
						'post_parent'		=>	$users_page
					)
				);
		
			}
		}
		
	}

}
