<?php
	

//$form_post, $donor_id, $donation_id

$donor = $this->get_donor_record($donor_id);
$donation = $this->get_donation_record($donation_id);

$donation_amount = $donation->donation_amount / 100;

$email_notification = $this->get_users_email_preference($_POST['personal-email']);

$prefs = $this->get_users_all_preference($donor->personal_preferences);

/*
var_dump($email_notification);

var_dump($_POST);

var_dump($donor);
var_dump($donation);
wp_die();
*/

$email_addresses = array();
$email_addresses[] = get_bloginfo('admin_email');
if($email_notification) {
	$email_addresses[] = $donor->donor_email;
}

// Email admin
$email_to = $email_addresses;
$admin_subject = get_bloginfo('name');


$admin_body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" style="font-size: 100%; font-family: Helvetica, Arial, sans-serif; line-height: 1.65; margin: 0; padding: 0;">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width" />
<!-- For development, pass document through inliner -->
</head>
  
<body style="font-size: 100%; font-family: Helvetica, Arial, sans-serif; line-height: 1.65; width: 100% !important; height: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; background: #efefef; margin: 0; padding: 0;" bgcolor="#efefef">
<table class="body-wrap" style="font-size: 100%; font-family: Helvetica, Arial, sans-serif; line-height: 1.65; width: 100% !important; height: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; background: #efefef; margin: 0; padding: 0;" bgcolor="#efefef">
	
	<tr style="font-size: 100%; font-family: Helvetica, Arial, sans-serif; line-height: 1.65; margin: 0; padding: 0;">
		<td class="container" style="font-size: 100%; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; display: block !important; clear: both !important; max-width: 780px !important; margin: 0 auto; padding: 0;">

			<!-- Message start -->
			<table style="font-size: 100%; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; width: 100% !important; border-collapse: collapse; margin: 0; padding: 0;">
				
				<tr style="font-size: 100%; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; margin: 0; padding: 0;">
				
					<td align="center" class="masthead" style="font-size: 100%; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; color: white; background: #2e3465; margin: 0; padding: 20px 20px;" bgcolor="#2e3465">
	
	                	<img src="http://newhope.squareonemd.net/wp-content/uploads/2016/10/hope-for-tomorrow-logo.jpg" alt="Hope for Tomorrow" style="font-size: 100%; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; max-width: 100%; display: block; margin: 0 auto; padding: 0;" />
	        		</td>
    			</tr>
				<tr style="font-size: 100%; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; margin: 0; padding: 0;">
					
					<td class="content" style="font-size: 100%; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; background: white; margin: 0; padding: 30px 35px;" bgcolor="white">

						
						
		                <table style="font-size: 100%; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; width: 100% !important; border-collapse: collapse; margin: 0; padding: 0;">
			                <tr style="font-size: 100%; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; margin: 0; padding: 0;">
			               		<td align="center" style="font-size: 100%; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; margin: 0; padding: 0;">
	                            	<p style="font-size: 16px; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; font-weight: normal; margin: 0 0 20px; padding: 0;">
		                            	<span class="button" style="font-size: 100%; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; color: white; text-decoration: none; display: inline-block; font-weight: bold; border-radius: 4px; background: #2e3465; margin: 0; padding: 0; border-color: #2e3465; border-style: solid; border-width: 10px 20px 8px;">Your donation ID is: '.$donation->hft_id.'</span>
									</p>
	                        	</td>
	                    	</tr>
	                    </table>
	                    

						<h2 style="font-size: 24px; font-family:Helvetica, Arial, sans-serif; line-height: 1.25; margin: 0px; padding: 0; color: #2e3465">Dear '.$donor->donor_name.',</h2>
						<h2 style="font-size: 24px; font-family:Helvetica, Arial, sans-serif; line-height: 1.25; margin: 0 0 20px; padding: 0; color: #2e3465">Thank you very much for your '.$donation->donation_type.' donation of &pound;'.number_format($donation_amount, 2).'</h2>
						
		                <p style="font-size: 16px; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; font-weight: normal; margin: 0 0 20px; padding: 0;"><strong style="font-size: 100%; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; margin: 0; padding: 0;">Donations are a vital part in building and maintaining our Mobile Chemotherapy Units.</strong></p>	                    

	                    
	                    
		                <table style="font-size: 100%; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; width: 100% !important; border-collapse: collapse; margin: 0 0 20px; padding: 0; border-radius: 4px; background: #FCF5C7;">
			                <tr style="font-size: 100%; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; margin: 0; padding: 0;">
			               		<td align="left" style="font-size: 100%; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; margin: 0; padding: 20px;">
					                <p style="font-size: 16px; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; font-weight: normal; margin: 0 0 0px; padding: 0;"><strong style="font-size: 100%; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; margin: 0; padding: 0;">Billing Address</strong></p>
					                
					                <p style="font-size: 16px; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; font-weight: normal; margin: 0 0 0px; padding: 0;">'.$donor->billing_address_line_1.'</p>
					                <p style="font-size: 16px; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; font-weight: normal; margin: 0 0 0px; padding: 0;">'.$donor->billing_city.'</p>
					                <p style="font-size: 16px; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; font-weight: normal; margin: 0 0 0px; padding: 0;">'.$donor->billing_zip.'</p>
					                <p style="font-size: 16px; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; font-weight: normal; margin: 0 0 20px; padding: 0;">'.$donor->billing_country.'</p>
					                
					                <p style="font-size: 16px; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; font-weight: normal; margin: 0 0 0px; padding: 0;"><strong style="font-size: 100%; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; margin: 0; padding: 0;">Your preferences included:</strong></p>
					                
					                '.$prefs.'

					                <p style="font-size: 16px; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; font-weight: normal; margin: 0 0 20px; padding: 0;">Gift Aid it: '.$donor->gift_aid.'</p>
					                
					                <p style="font-size: 16px; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; font-weight: normal; margin: 0 0 0px; padding: 0;"><strong style="font-size: 100%; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; margin: 0; padding: 0;">About Donation</strong></p>

					                <p style="font-size: 16px; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; font-weight: normal; margin: 0 0 20px; padding: 0;">'.$donor->hear_about_us_other.'</p>

					                <p style="font-size: 16px; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; font-weight: normal; margin: 0 0 0px; padding: 0;"><strong style="font-size: 100%; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; margin: 0; padding: 0;">Heard about Hope for Tomorrow</strong></p>

					                <p style="font-size: 16px; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; font-weight: normal; margin: 0 0 20px; padding: 0;">'.$donor->hear_about_us.'</p>

	                        	</td>
	                    	</tr>
	                    </table>
	                	                
	                    
                  
                  
	                    <p style="font-size: 16px; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; font-weight: normal; margin: 0 0 20px; padding: 0;">If you have any problems, please contact <a href="mailto:info@hopefortomorrow.org.uk" style="font-size: 100%; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; color: #71bc37; text-decoration: none; margin: 0; padding: 0;">info@hopefortomorrow.org.uk</a>.</p>
	
						<p style="font-size: 16px; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; font-weight: normal; margin: 0 0 20px; padding: 0;">Many thanks from all of the <em style="font-size: 100%; font-family:Helvetica, Arial, sans-serif; line-height: 1.65; margin: 0; padding: 0;">Hope for Tomorrow team!</em></p>
	
	            	</td>
        		</tr>
        	</table>
        </td>
	</tr>
	
	


</table>
</body>
</html>';

$admin_headers = array('Content-Type: text/html; charset=UTF-8');

wp_mail( $email_to, $admin_subject, $admin_body, $admin_headers );
?>