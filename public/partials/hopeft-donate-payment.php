<?php
/*
* Template Name: Donation Form
* @package Hope for Tomorrow
*/
$public_class = new Hopeft_Donate_Public('Hope Donation', '2.0.0');
$donation_form_fields = $public_class->get_donation_choices();
$public_pages = $public_class->public_pages();
$gccurrentplans = $public_class->gc_current_plans();

if ( ! isset( $_POST['donation_confirmation_field'] ) || ! wp_verify_nonce( $_POST['donation_confirmation_field'], 'donation_confirmation_nonce' ) ) {

   print 'Sorry, your nonce did not verify.';
   exit;

} else {
	
	if (isset($_POST['single']) && $_POST['single'] == '1'){
		$public_class->include_stripe_lib();
		$stripe = $public_class->get_stripe_credentials();
		\Stripe\Stripe::setApiKey($stripe['publishable_key']);
	}

	if (isset($_POST['monthly']) && $_POST['monthly'] == '1'){

		$client = $public_class->gocardless_init();
		
		$redirect_url = get_bloginfo( 'url' ) . $public_pages['donate-successful']['slug'];
		
		$redirectFlow = $client->redirectFlows()->create([
		    "params" => [
		        // This will be shown on the payment pages
		        "description" => "Hope for Tomorrow Donation",
		        // Not the access token
		        "session_token" => "dummy_session_token",
		        "success_redirect_url" => esc_html( $redirect_url ),
		        // Optionally, prefill customer details on the payment page
/*
		        "prefilled_customer" => [
		          "given_name" => "Tim",
		          "family_name" => "Rogers",
		          "email" => "tim@gocardless.com",
		          "address_line1" => "338-346 Goswell Road",
		          "city" => "London",
		          "postal_code" => "EC1V 7LQ"
		        ]
*/
		    ]
		]);
		
		$db_refID = $public_class->insert_gc_identifier($_POST, $redirectFlow->id);
	}

}

get_header(); ?>

<?php get_template_part( 'partials/featured-image' ); ?>

<?php //get_template_part( 'partials/tabbed-nav' ); ?>

<main id="main" class="site-main" role="main">
		
		
	<div class="content-container">
		
		<div class="progress">
			<div class="row">
				<div class="four columns">
					<p class="inactive">1. Your Donation</p>
				</div>
				<div class="four columns">
					<p class="active">2. Payment Details</p>
				</div>
				<div class="four columns">
					<p class="inactive">3. Thank You</p>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="twelve columns">
				
				<!-- Start the Loop. -->
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				
					<?php get_template_part('content-page'); ?>
					
					<section id="donation-payment" class="tabs">
										    
						<div class="confirm-donation">
							
							<?php if (isset($_POST['single']) && $_POST['single'] == '1') {  ?>
							
								<form method="post" action="<?php echo esc_attr( $public_pages['donate-successful']['slug']); ?>">
									
									<h3>Donation Details</h3>
									<p><strong>Type:</strong> <?php echo esc_html( ucfirst($_POST['donation-type']) );?> Donation</p>
									<p><strong>Amount:</strong> &pound;<?php echo esc_html(number_format($_POST['pence'],2));?> Donation</p>
									
									<div class="stripebtn">
										<script
											src="https://checkout.stripe.com/checkout.js" class="stripe-button"
											data-key="<?php echo esc_attr( $stripe['publishable_key'] );?>"
											data-amount="<?php echo esc_attr( $_POST['donation-amount'] ); ?>"
											data-email="<?php echo esc_attr( $_POST['email-address'] ); ?>"
											data-billing-address="true"
											data-name="Hope for Tomorrow"
											data-description="A Donation"
											data-image="<?php echo get_template_directory_uri() . '/images/hft-stripe-logo.jpg'; ?>"
											data-locale="auto"
											data-currency="gbp"
											stripeBillingName="<?php echo esc_attr( $_POST['full-name'] ); ?>"	
											>
										</script>
									</div>
									
									<?php $public_class->powered_by_stripe(); ?>
									
									<?php $public_class->hidden_billing_fields($donation_form_fields, $_POST); ?>
									
									<?php wp_nonce_field( 'donation_confirmation_nonce', 'donation_confirmation_field' ); ?>
								
								</form>
							
							<?php } ?>
							
							<?php if (isset($_POST['monthly']) && $_POST['monthly'] == '1') { ?>
							
								<h3>Donation Details</h3>
								<p><strong>Type:</strong> <?php echo esc_html( ucfirst($_POST['donation-type']) );?> Donation</p>
								<p><strong>Amount:</strong> &pound;<?php echo esc_html(number_format($_POST['pence'],2));?> Donation</p>
			
								<div class="stripebtn">
									
<!--
									<a href="<?php esc_html_e( $gccurrentplans[$_POST['pence']] ); ?>">
										<img src="http://gocardless-buttons.s3.amazonaws.com/v2/en/donate-with-gc-small@2x.png" width="250" height="37">
									</a>
-->
									
									<a href="<?php esc_html_e( $redirectFlow->redirect_url ); ?>">
										<img src="http://gocardless-buttons.s3.amazonaws.com/v2/en/donate-with-gc-small@2x.png" width="250" height="37">
									</a>
								
								</div>
							
							<?php } ?>
							
						
						</div>

					</section>
									
				
				
				<?php endwhile; else : ?>
					<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
				<?php endif; ?>
			</div>
		</div>
		
	</div>
		


</main>

<?php
	
get_footer(); ?>