<?php
/*
* Template Name: Donation Form
* @package Hope for Tomorrow
*/
$public_class = new Hopeft_Donate_Public('Hope Donation', '2.0.0');
$donation_form_fields = $public_class->get_donation_choices();
$public_pages = $public_class->public_pages();


get_header(); ?>

<?php get_template_part( 'partials/featured-image' ); ?>

<?php //get_template_part( 'partials/tabbed-nav' ); ?>

<main id="main" class="site-main" role="main">
		
	<div class="row">
		
		<div class="content-container">
			<div class="twelve columns">
				
				<!-- Start the Loop. -->
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				
					<?php get_template_part('content-page'); ?>
					
					<section id="donation-tab-nav" class="tabs">
					
					    <ul class="tab-nav">
					        <li class="active"><a href="#">Single Donation</a></li>
					        <li> <a href="#">Regular Donation</a></li>
					    </ul>
					
					    <div class="tab-content active">

							<form id="donation" method="post" action="<?php echo esc_attr( $public_pages['donate-confirmation']['slug']); ?>">
						    
							    <div class="row">
								    <div class="three columns">
									    
									    <div class="medium metro rounded btn primary predon"><a href="<?php echo wp_nonce_url( $public_pages['donate-confirmation']['slug'] .'?donation-amount=500&donation-type=single', 'donation_confirmation_nonce', 'donation_confirmation_field' ); ?>">&pound;5</a></div>
	
								    </div>
	
								    <div class="three columns">
									    
									    <div class="medium metro rounded btn primary predon"><a href="<?php echo wp_nonce_url( $public_pages['donate-confirmation']['slug'] .'?donation-amount=1000&donation-type=single', 'donation_confirmation_nonce', 'donation_confirmation_field' ); ?>">&pound;10</a></div>
	
								    </div>
	
								    <div class="three columns">
									    
									    <div class="medium metro rounded btn primary predon"><a href="<?php echo wp_nonce_url( $public_pages['donate-confirmation']['slug'] .'?donation-amount=2000&donation-type=single', 'donation_confirmation_nonce', 'donation_confirmation_field' ); ?>">&pound;20</a></div>
	
								    </div>
	
								    <div class="three columns">
									    
										<div class="field">
											<input id="other-amount" class="input" type="number" step="any" name="donation-amount-other" placeholder="other" />
										</div>
											
								    </div>
	
							    </div>
							    
							    <div class="row">
								    
									<?php wp_nonce_field( 'donation_confirmation_nonce', 'donation_confirmation_field' ); ?>
									<input class="input" type="hidden" name="donation-type" value="single" />
									
									<div id="donatenow" class="medium metro rounded btn primary" style="display: none;"><input type="submit" value="Donate now" /></div>

							    </div>
						    
						    </form>
					        
					    </div>
					    
					    <div class="tab-content">
						    
							<form id="donationdeets" method="post" action="<?php echo esc_attr( $public_pages['donate-confirmation']['slug']); ?>">
						    
							    <div class="row">
								    <div class="four columns">
									    
									    <div class="medium metro rounded btn primary"><a href="<?php echo wp_nonce_url( $public_pages['donate-confirmation']['slug'] .'?donation-amount=300&donation-type=monthly', 'donation_confirmation_nonce', 'donation_confirmation_field' ); ?>">&pound;3</a></div>
	
								    </div>
	
								    <div class="four columns">
									    
									    <div class="medium metro rounded btn primary"><a href="<?php echo wp_nonce_url( $public_pages['donate-confirmation']['slug'] .'?donation-amount=500&donation-type=monthly', 'donation_confirmation_nonce', 'donation_confirmation_field' ); ?>">&pound;5</a></div>
	
								    </div>
	
								    <div class="four columns">
									    
									    <div class="medium metro rounded btn primary"><a href="<?php echo wp_nonce_url( $public_pages['donate-confirmation']['slug'] .'?donation-amount=1000&donation-type=monthly', 'donation_confirmation_nonce', 'donation_confirmation_field' ); ?>">&pound;10</a></div>
	
								    </div>
		
							    </div>
							    
							    <div class="row">
								    
								    
								    
								    <p class="note">If you would like to donate a different monthly amount do please get in touch via telephone (<a href="tel:01666505055">01666 505055</a>) or via email (<a href="mailto:<?php echo antispambot( 'info@hopefortomorrow.org.uk');?>"><?php echo antispambot( 'info@hopefortomorrow.org.uk');?></a>) and we will happily arrange it for you.</p>
								    
									<?php wp_nonce_field( 'donation_confirmation_nonce', 'donation_confirmation_field' ); ?>
									<input class="input" type="hidden" name="donation-type" value="monthly" />
									
<!-- 									<div class="medium metro rounded btn primary"><input type="submit" value="Donate now" /></div> -->

							    </div>
						    
						    </form>

					    </div>
					    
					
					</section>
					
					
									
<!--
					<form id="donationdeets" method="post" action="<?php echo esc_attr( $public_pages['donate-confirmation']['slug']); ?>">
						
						<?php foreach ($donation_form_fields as $field) {  ?>
						
							
							
							<?php if ($field['type'] == 'select') { ?>
							
								<label for="<?php echo esc_attr( $field['id'] ); ?>"><?php echo $field['name']; ?></label>
								<div class="field">
									<div class="picker">
										<select name="<?php echo esc_attr( $field['id'] ); ?>">
											<option value="#" disabled><?php echo esc_attr( $field['name'] ); ?></option>
											<?php foreach ($field['options'] as $k => $v) { ?>
												<option value="<?php echo esc_attr( $k ); ?>"><?php echo esc_html( $v ); ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
							<?php } ?>
	
							<?php if ($field['type'] == 'radio') { ?>
							
								
								<div id="donateother" class="field">
									<label for="<?php echo esc_attr( $field['id'] ); ?>"><?php echo $field['name']; ?></label>
									<?php foreach ($field['options'] as $k => $v) { ?>
										<label class="radio" for="<?php echo esc_attr( $v ); ?>">
											<input name="<?php echo esc_attr( $field['id'] ); ?>" id="<?php echo esc_attr( $v ); ?>" value="<?php echo esc_attr( $k ); ?>" type="radio">
											<span></span> <?php echo esc_attr( $v ); ?>
										</label>
									<?php } ?>
								</div>
								<?php if ($field['hidden'] != '') {  ?>
									<div id="showdonateother" class="prepend field" style="display:none;">
										<span class="adjoined">&pound;</span>
										<input class="normal text input" name="<?php echo esc_attr( $field['hidden']['id'] ); ?>" type="<?php echo esc_attr( $field['hidden']['type'] ); ?>" steps="<?php echo esc_attr( $field['hidden']['steps'] ); ?>" min="<?php echo esc_attr( $field['hidden']['min'] ); ?>" placeholder="<?php echo esc_attr( $field['hidden']['name'] ); ?>"/>
									</div>
								<?php } ?>
							<?php } ?>
	
							<?php if ($field['type'] == 'checkbox') { ?>
								
								<label for="<?php echo esc_attr( $field['id'] ); ?>"><?php echo $field['name']; ?></label>
								
								<div class="field">
									<?php foreach ($field['options'] as $k => $v) { ?>
										<label class="checkbox checked" for="<?php echo esc_attr( $v ); ?>">
											<input name="<?php echo esc_attr( $field['id'] ); ?>[]" id="<?php echo esc_attr( $v ); ?>" value="<?php echo esc_attr( $k ); ?>" type="checkbox" checked="checked">
											<span></span> <?php echo esc_attr( $v ); ?>
										</label>
									<?php } ?>
								</div>
															
							<?php } ?>
	
							<?php if ($field['type'] == 'text') { ?>
								
								<label for="<?php echo esc_attr( $field['id'] ); ?>"><?php echo $field['name']; ?></label>
							
								<div class="field">
									<input class="input" type="text" name="<?php echo esc_attr( $field['id'] ); ?>" placeholder="<?php esc_attr( $field['name'] ); ?>" />
								</div>
							<?php } ?>
	
							<?php if ($field['type'] == 'textarea') { ?>
							
								<label for="<?php echo esc_attr( $field['id'] ); ?>"><?php echo $field['name']; ?></label>
							
								<div class="field">
									<textarea class="input textarea"  type="textarea" name="<?php echo esc_attr( $field['id'] ); ?>"></textarea>
								</div>
							<?php } ?>
	
	
							<?php if ($field['type'] == 'email') { ?>
								<div class="field">
									<input class="input" type="email" name="<?php echo esc_attr( $field['id'] ); ?>" placeholder="<?php echo esc_attr( $field['name'] ); ?>" />
								</div>
							<?php } ?>
													
						<?php } ?>
						
						<?php wp_nonce_field( 'donation_confirmation_nonce', 'donation_confirmation_field' ); ?>
						
						<div class="pretty medium primary btn"><input type="submit" value="Continue" /></div>
					
					</form>			
-->
				

				<?php endwhile; else : ?>
					<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
				<?php endif; ?>
				
				<div class="row">
					<div class="twelve columns">
						<h1 class="entry-title donation-type">Other ways to give</h1>
					</div>
				</div>
				<div class="row">
					<div class="six columns">
						<p><strong>TEXT</strong></p>
						<p>Text HOPE to 70660 to make a donation of £5</p>
						<p><strong>PHONE</strong></p>
						<p>Call 01666 505055. All major credit cards accepted except Diners.</p>
					</div>
					<div class="six columns">
						<p><strong>BY POST</strong></p>
						<p>Download the donation forms and send them back to us by post, the return address is detailed in the download.</p>
						<?php $text_to_be_wrapped_in_shortcode = '<a href="'.get_bloginfo( 'url' ).'/wp-content/uploads/2017/08/hopefortomorrowdonationforms.zip">Donation Forms</a>';
							echo do_shortcode('[bigbuttondownload]' . $text_to_be_wrapped_in_shortcode . '[/bigbuttondownload]'); ?>
					</div>
				</div>
				
				

				
			</div>
		</div>
	</div>


</main>

<?php get_footer(); ?>