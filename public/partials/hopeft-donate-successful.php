<?php
/**
* Template Name: Confirm Donation
* @package Hope for Tomorrow
*/
$public_class = new Hopeft_Donate_Public('Hope Donation', '2.0.0');

if (isset($_POST) && !empty($_POST)) {
	$public_class->make_charge($_POST);
} elseif (isset($_GET) && $_GET['redirect_flow_id'] != '') {
	$public_class->create_mandate_customer($_GET);
}

get_header(); ?>

	<main id="main" class="site-main" role="main">
		
		
		<div class="row">
			
			<div class="content-container success">
				<div class="twelve columns">
					
					<h2 class="thanksuccess">Thank you for choosing to donate to Hope for Tomorrow.</h2>
					<p><strong>Your donation represents a vital contribution to Hope for Tomorrow’s ongoing Mobile
Chemotherapy Unit Project and will be used where the need is greatest to help bring cancer care closer to patients’ homes.<strong></p>

				</div>
			</div>
		</div>

		<?php //get_template_part('partials/patient-stories'); ?>

		<?php //get_template_part('partials/our-units'); ?>

		<?php //get_template_part('partials/news-and-events'); ?>

		<?php //get_template_part('partials/newsletter-container'); ?>

	</main>

<?php get_footer(); ?>


