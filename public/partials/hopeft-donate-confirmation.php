<?php
/**
* Template Name: Confirm Donation
* @package Hope for Tomorrow
*/
$public_class = new Hopeft_Donate_Public('Hope Donation', '2.0.0');
$donation_form_fields = $public_class->get_donation_choices();
$public_pages = $public_class->public_pages();


// a hack to trigger email notification test
// $public_class->donation_notification($_POST, 5, 30);

get_header(); ?>

	<main id="main" class="site-main" role="main">
		
<?php

if (isset($_POST) && !empty($_POST)) {
	$donation_vars = $_POST;
}
if (isset($_GET) && !empty($_GET)) {
	$donation_vars = $_GET;
}

	
if ( ! isset( $donation_vars['donation_confirmation_field'] ) || ! wp_verify_nonce( $donation_vars['donation_confirmation_field'], 'donation_confirmation_nonce' ) ) {

   print 'Sorry, your nonce did not verify.';
   exit;

} else {

	$retarray = $public_class->process_donation_step_one($donation_vars);
}

?>

<div class="content-container">
	
	<div class="progress">
		<div class="row">
			<div class="four columns">
				<p class="active">1. Your Donation</p>
			</div>
			<div class="four columns">
				<p class="inactive">2. Payment Details</p>
			</div>
			<div class="four columns">
				<p class="inactive">3. Thank You</p>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="eight columns">
			<h1 class="entry-title donation-type"><?php echo esc_html( ucfirst($retarray['donation-type']) );?> Donation</h1>
			<div class="damount">
				&pound;<?php echo esc_html(number_format($retarray['pence'],2));?>
			</div>
			
			
<!-- 			<form id="donationdeets" method="post" action="<?php echo esc_attr( $public_pages['donate-confirmation']['slug']); ?>"> -->
				
			<form id="donationdeets" method="post" action="<?php echo esc_attr( $public_pages['donate-payment']['slug']); ?>">
				
				<?php foreach ($donation_form_fields as $field) {  ?>

					<?php if ($field['type'] == 'select') { ?>
					
						<label for="<?php echo esc_attr( $field['id'] ); ?>"><?php echo $field['name']; ?></label>
						<div class="field">
							<div class="picker">
								<select name="<?php echo esc_attr( $field['id'] ); ?>">
									<option value="#" disabled><?php echo esc_attr( $field['name'] ); ?></option>
									<?php foreach ($field['options'] as $k => $v) { ?>
										<option value="<?php echo esc_attr( $k ); ?>"><?php echo esc_html( $v ); ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					<?php } ?>

					<?php if ($field['type'] == 'radio') { ?>
					
					
						
						<div id="donateother" class="field">
							<label for="<?php echo esc_attr( $field['id'] ); ?>"><?php echo $field['name']; ?></label>
							<?php foreach ($field['options'] as $k => $v) { ?>
								<label class="radio" for="<?php echo esc_attr( $v ); ?>">
									<input name="<?php echo esc_attr( $field['id'] ); ?>" id="<?php echo esc_attr( $v ); ?>" value="<?php echo esc_attr( $k ); ?>" type="radio">
									<span></span> <?php echo esc_attr( $v ); ?>
								</label>
							<?php } ?>
						</div>
						
						<?php if ($field['hidden'] != '') {  ?>
							<div id="showdonateother" class="prepend field" style="display:none;">
								<span class="adjoined">&pound;</span>
								<input class="normal text input" name="<?php echo esc_attr( $field['hidden']['id'] ); ?>" type="<?php echo esc_attr( $field['hidden']['type'] ); ?>" steps="<?php echo esc_attr( $field['hidden']['steps'] ); ?>" min="<?php echo esc_attr( $field['hidden']['min'] ); ?>" placeholder="<?php echo esc_attr( $field['hidden']['name'] ); ?>"/>
							</div>
						<?php } ?>
					<?php } ?>

					<?php if ($field['type'] == 'checkbox' && $field['id'] == 'gift_aid') { ?>
						
						<div class="row">
							<div class="four columns">
								<?php $public_class->giftaidit(); ?>
							</div>
							<div class="eight columns">
								<p>If you are a UK taxpayer, tick the <strong>GIFT AID</strong> box and increase the  value of your donation by <strong>25p for every £1</strong> you give at no extra cost to you.</p>
							</div>
						</div>
						
						<div class="field">
							<?php foreach ($field['options'] as $k => $v) { ?>
								<label class="checkbox" for="<?php echo esc_attr( $v ); ?>">
									<input name="<?php echo esc_attr( $field['id'] ); ?>[]" id="<?php echo esc_attr( $v ); ?>" value="<?php echo esc_attr( $k ); ?>" type="checkbox">
									<span></span> <?php echo esc_attr( $v ); ?>
								</label>
							<?php } ?>
						</div>
						
						<p><em>I am a UK  taxpayer and understand that if I pay less Income Tax and/or capital gains tax than the amount of gift aid claimed on  all  my donations in that tax year it is  my responsibility to pay any difference.</em></p>
						
						<hr>
																		
					<?php } ?>

					<?php if ($field['type'] == 'checkbox' && $field['id'] != 'gift_aid') { ?>
						
						<?php if ($field['desc']) { ?>
							<p><strong><?php echo esc_attr( $field['desc'] ); ?></strong></p>
						<?php } ?>
						<?php if ($field['name']) { ?>
							<p><strong><?php echo esc_attr( $field['name'] ); ?></strong></p>
						<?php } ?>
						
						<div class="field">
							<?php foreach ($field['options'] as $k => $v) { ?>
								<label class="checkbox" for="<?php echo esc_attr( $v ); ?>">
									<input name="<?php echo esc_attr( $field['id'] ); ?>[]" id="<?php echo esc_attr( $v ); ?>" value="<?php echo esc_attr( $k ); ?>" type="checkbox">
									<span></span> <?php echo esc_attr( $v ); ?>
								</label>
							<?php } ?>
						</div>
						
						<hr>

					<?php } ?>

					<?php if ($field['type'] == 'text') { ?>
						
					
						<div class="field">
							<input class="input" type="text" name="<?php echo esc_attr( $field['id'] ); ?>" placeholder="<?php echo esc_attr( $field['name'] ); ?>" />
						</div>
					<?php } ?>

					<?php if ($field['type'] == 'textarea') { ?>
					
						<div class="field">
							<textarea class="input textarea" placeholder="<?php echo esc_attr($field['name']); ?>" type="textarea" name="<?php echo esc_attr( $field['id'] ); ?>"></textarea>
						</div>
					<?php } ?>


					<?php if ($field['type'] == 'email') { ?>
						<div class="field">
							<input class="input" type="email" name="<?php echo esc_attr( $field['id'] ); ?>" placeholder="<?php echo esc_attr( $field['name'] ); ?>" />
						</div>
					<?php } ?>
											
				<?php } ?>
				
				
				<?php foreach ($retarray as $k => $donation_fields) { ?>
						<input class="input" type="hidden" name="<?php echo esc_attr( $k ); ?>" value="<?php echo esc_attr( $donation_fields ); ?>" />
				<?php } ?>
				
				<?php wp_nonce_field( 'donation_confirmation_nonce', 'donation_confirmation_field' ); ?>
				
				<div id="bigbut">
					<div class="medium metro rounded btn primary"><input type="submit" value="Proceed to payment" /></div>
				</div>
				
				<div class="ddstatement">
					<p><strong>DATA PROTECTION STATEMENT</strong></p>
					<p>Hope for  Tomorrow is committed to protecting your privacy and will  process your personal data in  accordance with the Data Protection Act 1998. Hope for Tomorrow will not share, sell or rent your personal information to third parties for marketing purposes.</p>
					
				</div>
			
			</form>			

		</div>

		<div class="four columns">
		</div>

	</div>
	
</div>
	
	
	
	
	
















<!--
<div class="row">
	
	<div class="content-container">

		<div class="four columns">
			
			<div class="confirm-donation">
				
				<?php if (isset($retarray['single']) && $retarray['single'] == true) {  ?>
				
					<form method="post" action="<?php echo esc_attr( $public_pages['donate-successful']['slug']); ?>">
						
						<h3>Donation Details</h3>
						<p><strong>Type:</strong> <?php echo esc_html( ucfirst($retarray['donation-type']) );?> Donation</p>
						<p><strong>Amount:</strong> &pound;<?php echo esc_html(number_format($retarray['pence'],2));?> Donation</p>
						
						<div class="stripebtn">
							<script
								src="https://checkout.stripe.com/checkout.js" class="stripe-button"
								data-key="<?php echo esc_attr( $stripe['publishable_key'] );?>"
								data-amount="<?php echo esc_attr( $retarray['donation-amount'] ); ?>"
								data-email="<?php echo esc_attr( $_POST['email-address'] ); ?>"
								data-billing-address="true"
								data-name="Hope for Tomorrow"
								data-description="A Donation"
								data-image="<?php echo get_template_directory_uri() . '/images/hft-stripe-logo.jpg'; ?>"
								data-locale="auto"
								data-currency="gbp">
							</script>
						</div>
						
						<?php $public_class->powered_by_stripe(); ?>
						
						<?php $public_class->hidden_billing_fields($donation_form_fields, $_POST); ?>
						
						<?php wp_nonce_field( 'donation_confirmation_nonce', 'donation_confirmation_field' ); ?>
					
					</form>
				
				<?php } ?>
				
				<?php if (isset($retarray['monthly']) && $retarray['monthly'] == true) { ?>
				
					<h3>Donation Details</h3>
					<p><strong>Type:</strong> <?php echo esc_html( ucfirst($retarray['donation-type']) );?> Donation</p>
					<p><strong>Amount:</strong> &pound;<?php echo esc_html(number_format($retarray['pence'],2));?> Donation</p>

					<div class="stripebtn">
						
						<a href="<?php esc_html_e( $redirectFlow->redirect_url ); ?>">
							<img src="http://gocardless-buttons.s3.amazonaws.com/v2/en/donate-with-gc-small@2x.png" width="250" height="37">
						</a>
					
					</div>
				
				<?php } ?>
				
			
			</div>

		
		</div>	
		<div class="eight columns">
			
			 <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			
			 	<?php get_template_part('content-page'); ?>
			
			 <?php endwhile; else : ?>
			 	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			 <?php endif; ?>
			
			<div class="billing-details">
				<ul>
				<?php $public_class->confirm_billing_details($donation_form_fields, $_POST); ?>
				</ul>
			</div>
			
		</div>
	</div>
</div>
-->

		<?php //get_template_part('partials/patient-stories'); ?>

		<?php //get_template_part('partials/our-units'); ?>

		<?php //get_template_part('partials/news-and-events'); ?>

		<?php //get_template_part('partials/newsletter-container'); ?>

	</main>

<?php get_footer(); ?>


