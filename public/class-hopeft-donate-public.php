<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.squareonemd.co.uk
 * @since      2.0.0
 *
 * @package    Hopeft_Donate
 * @subpackage Hopeft_Donate/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Hopeft_Donate
 * @subpackage Hopeft_Donate/public
 * @author     Your Name <email@example.com>
 */
class Hopeft_Donate_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    2.0.0
	 * @access   private
	 * @var      string    $hopeft_donate    The ID of this plugin.
	 */
	private $hopeft_donate;

	/**
	 * The version of this plugin.
	 *
	 * @since    2.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    2.0.0
	 * @param      string    $hopeft_donate       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $hopeft_donate, $version ) {

		$this->hopeft_donate = $hopeft_donate;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    2.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Hopeft_Donate_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Hopeft_Donate_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->hopeft_donate, plugin_dir_url( __FILE__ ) . 'css/hopeft-donate-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    2.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Hopeft_Donate_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Hopeft_Donate_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->hopeft_donate, plugin_dir_url( __FILE__ ) . 'js/hopeft-donate-public.js', array( 'jquery' ), $this->version, false );

	}
	
	/**
	 * A function to return all the donation pages we need.
	 *
	 * @since    2.0.0
	 * @return   $array    $array    an array of pages and there associated slugs.
	 */	
	public function public_pages() {
		$array = array(
			'donate-hope-for-tomorrow' => array(
				'slug' => '/donate-hope-for-tomorrow/',
				'template' => 'partials/hopeft-donate.php'
			),
			'donate-details' => array(
				'slug' => '/donate-hope-for-tomorrow/donate-details/',
				'template' => 'partials/hopeft-donate-details.php'
			),
			'donate-confirmation' => array(
				'slug' => '/donate-hope-for-tomorrow/donate-confirmation/',
				'template' => 'partials/hopeft-donate-confirmation.php'
			),
			'donate-payment' => array(
				'slug' => '/donate-hope-for-tomorrow/donate-payment/',
				'template' => 'partials/hopeft-donate-payment.php'
			),
			'donate-successful' => array(
				'slug' => '/donate-hope-for-tomorrow/donate-successful/',
				'template' => 'partials/hopeft-donate-successful.php'
			)
		);
		return $array;
	}
	
	/**
	 * A function to return a page as included via this plugin.
	 *
	 * @since    2.0.0
	 * @param    string    $original_template    the path of the original template.
	 * @return   string    $original_template    the conditionally filtered path of the template.
	 */	
	public function page_includes( $original_template ) {
		
		global $post;		
		$public_pages = $this->public_pages();
		
		foreach ($public_pages as $k => $public_page) {
			if ( is_page( $k ) ) {
				$original_template = plugin_dir_path( __FILE__ ) . $public_page['template'];
			}
		}

		return $original_template;
	}
	
	/**
	 * Retrieves a list of donation choices.
	 *
	 * @param N/A
	 * @return array An array of form fields types.
	 *
	 */
	public function get_donation_choices() {
		$donation_choices_fields = array(
			array(
				'id'   => 'full-name',
				'name' => 'Full Name',
				'type' => 'text',
				'required' => true,
			),
			array(
				'id'   => 'billing-address-line-1',
				'name' => 'Address Line 1',
				'type' => 'text',
				'required' => true,
			),
			array(
				'id'   => 'billing-address-line-2',
				'name' => 'Address Line 2',
				'type' => 'text',
				'required' => true,
			),
			array(
				'id'   => 'billing-address-line-3',
				'name' => 'Address Line 3',
				'type' => 'text',
				'required' => true,
			),
			array(
				'id'   => 'billing-address-town',
				'name' => 'Town',
				'type' => 'text',
				'required' => true,
			),
			array(
				'id'   => 'billing-address-county',
				'name' => 'County',
				'type' => 'text',
				'required' => true,
			),
			array(
				'id'   => 'billing-address-postcode',
				'name' => 'Postcode',
				'type' => 'text',
				'required' => true,
			),
			array(
				'id'   => 'email-address',
				'name' => 'Email',
				'type' => 'email',
				'required' => true,
			),
			array(
				'id'   => 'about-your-donation',
				'name' => 'Tell us about your donation',
				'type' => 'textarea',
			),
			array(
				'id'   => 'hear-about-us',
				'name' => 'How did you hear about us?',
				'type' => 'textarea',
			),
			array(
				'id'   => 'gift_aid',
				'name' => 'Gift Aid',
				'type' => 'checkbox',
				'options' => array(
					'yes' => 'I want to Gift Aid my donation and any donations I make in the  future or have  made in the past four years.',
				)
			),
			array(
				'id'   => 'personal-preferences',
				'name' => 'We would love to stay in touch and tell you about how your donation is helping cancer patients across the country.  If you would rather not be contacted please tick here',
				'type' => 'checkbox',
				'options' => array(
					'no-contact' => 'No contact thanks',
				)
			),
		);
		return $donation_choices_fields;
	}
	
	public function process_donation_step_one($post_vars) {
		
		$retarray = array();
		$retarray['donation-type'] = $post_vars['donation-type'];
		
		if (isset($post_vars['donation-amount-other']) && !empty($post_vars['donation-amount-other'])) {
			$donation_amount = (float)$post_vars['donation-amount-other'] * 100;
			$retarray['pence'] = $donation_amount / 100;
			$retarray['donation-amount'] = $donation_amount;
		} else {
			$donation_amount = $post_vars['donation-amount'];
			$retarray['pence'] = $donation_amount / 100;
			$retarray['donation-amount'] = $donation_amount;
		}
		
		if ($post_vars['donation-type'] == 'single') {
			$retarray['single'] = true;	
		}
		
		if ($post_vars['donation-type'] == 'monthly') {
			$retarray['monthly'] = true;				
		}

		return $retarray;

	}
	
	/**
	 * reset the from email address.
	 *
	 * @since    2.0.0
	 * @param  	 string	  $original_email_address
	 * @return   string   the email address for the from field.
	 */
	public function hft_wp_mail_from( $original_email_address ) {
		//Make sure the email is from the same domain 
		//as your website to avoid being marked as spam.
		$admin_email = get_bloginfo('admin_email');
		return $admin_email;
	}
	
	/**
	 * reset the name when an email triggered.
	 *
	 * @since    2.0.0
	 * @param  	 string	  $original_email_from
	 * @return   string   the email address for the from field.
	 */
	public function hft_wp_mail_from_name( $original_email_from ) {
		$site_title = get_bloginfo('name');
		return $site_title;
	}

	/**
	 * include the stripe library.
	 *
	 * @return does nothing but the include the lib.
	 *
	 */
	public function include_stripe_lib() {
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/libs/stripe-php-4.4.2/init.php';
	}
	
	/**
	 * include the gocardless library.
	 *
	 * @return does nothing but the include the lib.
	 *
	 */
	public function include_gocardless_lib() {
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/libs/vendor/autoload.php';
	}
	
	public function powered_by_stripe() {
		echo '<div class="poweredby"><img src="' . plugin_dir_url( __FILE__ ) . 'images/secure-stripe-payment-logo.png" alt="powered_by_stripe"></div>';
	}
	
	public function giftaidit() {
		echo '<div class="giftaidit"><img src="' . plugin_dir_url( __FILE__ ) . 'images/giftaid.png" alt="giftaid it"></div>';
	}
	
	/**
	 * Set stripe mode.
	 *
	 * @return string Setting this to 'test' will take Stripe payment out of live mode.
	 *
	 */
	public function	get_stripe_mode() {
		
		// comment this out for switch to live.
		// uncomment this to put in test mode.
		// $mode = 'test';
		return $mode;
	}
	
	/**
	 * Retrieves stripe lib.
	 *
	 * @return array An array of the credentials.
	 *
	 */
	public function get_stripe_credentials() {
		
		$mode = $this->get_stripe_mode();
		
		$test_credentials = array(
			'secret_key' => 'sk_test_GzWzuNUpSLMwZXdr1cjIX4Y0',
			'publishable_key' => 'pk_test_TmZSTLAzqQAe2eTrK4dHXc5C'
		);
		
		$live_credentials = array(
			'secret_key' => 'sk_live_vnqg3VMzDxJRG64PlYGX11xS',
			'publishable_key' => 'pk_live_yCpCqWsHjjAmVwEG28SOUPuo'
		);
		
		if ($mode == 'test') {
			$stripe = $test_credentials;
		} else {
			$stripe = $live_credentials;
		}
		
		return $stripe;
	}

	/**
	 * Retrieves billing information.
	 *
	 * @param	array	$donation_form_fields the array of form fields
	 * @return	array	$form_post the array from a global $_POST variable.
	 *
	 */
	public function confirm_billing_details($donation_form_fields, $form_post) {
			
		foreach($donation_form_fields as $form_field) {
			foreach ($form_post as $k => $v) {
				
				if ($form_field['id'] == $k && !empty($v)) {
					if(is_array($v)) {
						echo '<span class="labelled">' . esc_html( $form_field['name']) . ':</span>';
						foreach ($v as $i => $value) {
							echo '<li><span class="labelled"></span>'.esc_html( $form_field['options'][$value] ).'</li>';
						}
					} else {
						if ($k == 'donation-amount' && $v != 'other') {
							$v = $v / 100;
							echo '<li><span class="labelled">' . esc_html( $form_field['name'] ) . ':</span> &pound;'.esc_html( number_format( $v, 2 ) ).'</li>';
						} else {
							echo '<li><span class="labelled">' . esc_html( $form_field['name'] ) . ':</span> '.esc_html( $v ).'</li>';
						}
						if ($k == 'donation-amount' && $v == 'other') {
							echo '<li><span class="labelled">' . esc_html( $form_field['name'] ) . ':</span> &pound;'.esc_html( number_format( $form_post['donation-amount-other'], 2 ) ).'</li>';
						}
						
					}
					
					
				}

			}
		}
	
	}	

	/**
	 * Retrieves billing information.
	 *
	 * @param	array	$donation_form_fields the array of form fields
	 * @return	array	$form_post the array from a global $_POST variable.
	 *
	 */
	public function hidden_billing_fields($donation_form_fields, $form_post) {
		
		foreach($donation_form_fields as $form_field) {
			
			foreach ($form_post as $k => $v) {
				if ($form_field['id'] == $k && !empty($v)) {
					
					if(is_array($v)) {
						foreach ($v as $i => $value) {
							echo '<input type="hidden" name="'.$form_field['id'].'[]" value="'.$value.'">';
						}
					} elseif($k == 'donation-amount' && $v == 'other') {
						// this compensates for selecting amount as "other" then adds the users custom input donation
						$v = $form_post['donation-amount-other'] * 100;
						echo '<input type="hidden" name="' . esc_html( $form_field['id'] ) . '" value="'.esc_html( $v ).'">';
					} else {
						echo '<input type="hidden" name="' . esc_html( $form_field['id'] ) . '" value="'.esc_html( $v ).'">';
					}
				}
			}
		}
		
		echo '<input type="hidden" name="donation-type" value="'.esc_html( $form_post['donation-type'] ).'">';		
		echo '<input type="hidden" name="donation-amount" value="'.esc_html( $form_post['donation-amount'] ).'">';		
	
	}
	
	/**
	 * Complete the purchase and charge for the donation.
	 * NOTE: if the donation is monthly a separate function is fired on the page
	 * 		 so there is no need to cover the montly in this function
	 *
	 * @param	array	$form_post the global $_POST array
	 *
	 */
	public function make_charge($form_post) {
		
		if ( 
		    ! isset( $form_post['donation_confirmation_field'] ) 
		    || ! wp_verify_nonce( $form_post['donation_confirmation_field'], 'donation_confirmation_nonce' ) 
		) {
		
		   print 'Sorry, your nonce did not verify.';
		   exit;
		
		} else {
			
			$this->include_stripe_lib();
			$stripe = $this->get_stripe_credentials();
			
			\Stripe\Stripe::setApiKey($stripe['secret_key']);
			$pence = $form_post['donation-amount'] / 100;
			
			if ($form_post['donation-type'] == 'monthly') {
				
				//var_dump($form_post);
				
			}
			if ($_POST['donation-type'] == 'single') {
				
				//var_dump($form_post);
				
				// Token is created using Stripe.js or Checkout!
				// Get the payment token submitted by the form:
				$token = $form_post['stripeToken'];
				  				
				// Charge the user's card:
				$charge = \Stripe\Charge::create(array(
					"amount" => $form_post['donation-amount'],
					"currency" => "gbp",
					"description" => "Single Donation",
					"source" => $token,
				));
				
				//var_dump($charge);
				$donor_id = $this->insert_donor($form_post);
				$donation_id = $this->insert_donation($form_post, $donor_id, $charge->id);
				$this->update_donor_donation_id($donor_id, $donation_id);
				
				$this->donation_notification($form_post, $donor_id, $donation_id);

			}
			
		}
	}
	
	/**
	 * Create a new client object via GoCardless.
	 *
	 * @return	object	$client the object used to spin off all GC methods.
	 *
	 */
	public function gocardless_init() {
		
		$this->include_gocardless_lib();
		
		$client = new \GoCardlessPro\Client([
		    // We recommend storing your access token in an environment variable for security, but you could include it as a string directly in your code
		    'access_token' => $this->hft_gocardless_access_token(),
		    // Change me to LIVE when you're ready to go live
		    'environment' => \GoCardlessPro\Environment::LIVE
		]);
		
		return $client;
	}
	
	public function create_mandate_customer($global_get) {
		
		
		$client = $this->gocardless_init();
		
		$redirectFlow = $client->redirectFlows()->complete(
		    $global_get['redirect_flow_id'], //The redirect flow ID from above.
		    ["params" => ["session_token" => "dummy_session_token"]]
		);
		
		// 
		
/*
		print("Mandate: " . $redirectFlow->links->mandate . "<br />");
		// Save this mandate ID for the next section.
		print("Customer: " . $redirectFlow->links->customer . "<br />");
*/
		
		$customer = $client->customers()->get($redirectFlow->links->customer);
		$mandate = $client->mandates()->get($redirectFlow->links->mandate);
		
		$amount = $this->get_gc_amount($global_get['redirect_flow_id']);
		
		$this->create_gc_charge($amount, $redirectFlow->links->mandate, $global_get['redirect_flow_id']);
		
		$donor_array = $this->create_donor_array($customer);
		$donor_id = $this->insert_donor($donor_array);
		
		$donation_id = $this->get_gc_id($global_get['redirect_flow_id']);
		$this->update_donor_donation_id($donor_id, $donation_id);
		
		$this->update_mandate_id($donation_id, $redirectFlow->links->mandate);
		$this->update_gc_customer_id($donor_id, $redirectFlow->links->customer);
		$this->update_gc_customer_prefs($donor_id, $donation_id);

		$this->donation_notification($donor_array, $donor_id, $donation_id);
		
		//var_dump($customer);
		//var_dump($mandate);
		
	}
	
	public function update_gc_customer_prefs($donor_id, $donation_id) {
		global $wpdb; // this is how you get access to the database
			
		$donors = $wpdb->prefix . 'hft_donors';
		$donations = $wpdb->prefix . 'hft_donations';
		
		$donation = $wpdb->get_row(
			$wpdb->prepare(
				"
					SELECT * FROM $donations
					WHERE donation_id = %s
				",
				array(
					$donation_id
				)
			)
		);
	
		$gc_donation_prefs = $donation->personal_preferences;
		$gift_aid = $donation->gift_aid;
		$heard_about_hft = $donation->hear_about_us;
		$about_donation = $donation->hear_about_us_other;

		$wpdb->query( $wpdb->prepare( 
			"
				UPDATE $donors
				SET personal_preferences = %s, gift_aid = %s, hear_about_us = %s, hear_about_us_other = %s
				WHERE donor_id = %s
			",
			array(
				$gc_donation_prefs,$gift_aid,$heard_about_hft,$about_donation,$donor_id
			)
		) );

	}
	
	public function update_mandate_id($donation_id, $mandate_id) {
		global $wpdb; // this is how you get access to the database
		$donations = $wpdb->prefix . 'hft_donations';
		$wpdb->query( $wpdb->prepare( 
			"
				UPDATE $donations
				SET gc_mandate_identifier = %s
				WHERE donation_id = %s
			",
			array(
				$mandate_id,$donation_id
			)
		) );
	}
	
	public function update_gc_customer_id($donor_id, $customer_id) {
		
/*
		var_dump($donor_id);
		var_dump($customer_id);
*/
		
		
		global $wpdb; // this is how you get access to the database
		$donors = $wpdb->prefix . 'hft_donors';
		$wpdb->query( $wpdb->prepare( 
			"
				UPDATE $donors
				SET gc_customer_id = %s
				WHERE donor_id = %s
			",
			array(
				$customer_id,$donor_id
			)
		) );
	}
	
	public function create_donor_array($customer) {

		$donor_array = array();
		$donor_array['stripeBillingName'] = $customer->given_name . ' ' . $customer->family_name;
		$donor_array['stripeEmail'] = $customer->email;
		$donor_array['stripeBillingAddressLine1'] = $customer->address_line1;
		$donor_array['stripeBillingAddressZip'] = $customer->postal_code;
		$donor_array['stripeBillingAddressCity'] = $customer->city;
		$donor_array['stripeBillingAddressCountryCode'] = $customer->country_code;
		
		return $donor_array;
	}
	
	public function get_gc_id($redirect_flow_id) {
		global $wpdb; // this is how you get access to the database
			
		$table_name = $wpdb->prefix . 'hft_donations';

		$donation_exists = $wpdb->get_row(
			$wpdb->prepare(
				"
					SELECT * FROM $table_name
					WHERE gc_redirect_identifier = %s
				",
				array(
					$redirect_flow_id
				)
			)
		);
	
		return $donation_exists->donation_id;
	}
	
	public function get_gc_amount($redirect_flow_id) {
		
		global $wpdb; // this is how you get access to the database
			
		$table_name = $wpdb->prefix . 'hft_donations';

		$donation_exists = $wpdb->get_row(
			$wpdb->prepare(
				"
					SELECT * FROM $table_name
					WHERE gc_redirect_identifier = %s
				",
				array(
					$redirect_flow_id
				)
			)
		);
		
		$amount = $donation_exists->donation_amount;
		
		return $amount;
	}
	
	public function create_gc_charge($amount, $mandate, $redirect_flow_id){
		
		$client = $this->gocardless_init();
		
		$errors = array();

		try {
			$subscription = $client->subscriptions()->create([
			  "params" => [
			      "amount" => $amount, // 15 GBP in pence
			      "currency" => "GBP",
			      "interval_unit" => "monthly",
			      "day_of_month" => "7",
			      "links" => [
			          "mandate" => $mandate
			                       // Mandate ID from the last section
			      ],
			      "metadata" => [
			          "subscription_source" => "WEBSITE"
			      ]
			  ]
			]);
			
		} catch (\GoCardlessPro\Core\Exception\ApiException $e) {
			// Api request failed / record couldn't be created.
			$errors[] = $e;
		} catch (\GoCardlessPro\Core\Exception\MalformedResponseException $e) {
			// Unexpected non-JSON response
			$errors[] = $e;
		} catch (\GoCardlessPro\Core\Exception\ApiConnectionException $e) {
			// Network error
			$errors[] = $e;
		}
		
		if(!empty($errors)) {
			$errorstring = implode("\n", $errors);
			wp_mail( 'elliott@squareonemd.co.uk', 'Hopefortomorrow GoCardless Error', $errorstring );
		}
		
		$donation_id = $this->get_gc_id($redirect_flow_id);
		
		$this->update_donation_subscription_identifier($donation_id, $subscription->id);
		
		// Keep hold of this subscription ID - we'll use it in a minute.
		// It should look a bit like "SB00003GKMHFFY"
		//print("ID: " . $subscription->id);

	}
	
	public function update_donation_subscription_identifier($donation_id, $subscription_id) {
/*
		
		var_dump($donation_id);
		var_dump($subscription_id);
*/
		
		global $wpdb; // this is how you get access to the database
		$donations = $wpdb->prefix . 'hft_donations';
		$wpdb->query( $wpdb->prepare( 
			"
				UPDATE $donations
				SET gc_subscription_identifier = %s
				WHERE donation_id = %s
			",
			array(
				$subscription_id,$donation_id
			)
		) );
	}
	
	////////// comment these db queries ////////////
	//			There is currently no capture for all the perifereal data ////
	//			perhaps should be caught in a serialised array	///
	public function update_donor_donation_id($donor_id, $donation_id) {
		global $wpdb; // this is how you get access to the database
			
		$donors = $wpdb->prefix . 'hft_donors';
		$donations = $wpdb->prefix . 'hft_donations';
		$hft_id = $donation_id.date('my', strtotime('now'));
		
		$wpdb->query( $wpdb->prepare( 
			"
				UPDATE $donors
				SET donation_id = %s
				WHERE donor_id = %s
			",
			array(
				$donation_id,$donor_id
			)
		) );

		$wpdb->query( $wpdb->prepare( 
			"
				UPDATE $donations
				SET hft_id = %s, donor_id = %s
				WHERE donation_id = %s
			",
			array(
				$hft_id,$donor_id,$donation_id
			)
		) );
		
	}
	
	public function insert_gc_identifier($form_post, $redirect_id) {
		

		global $wpdb; // this is how you get access to the database
			
		$table_name = $wpdb->prefix . 'hft_donations';
			
		$timenow = date('Y-m-d H:i:s', strtotime('now'));
		
		
		$donation_type = sanitize_text_field( $form_post['donation-type'] );
		if ($form_post['donation-amount'] == 'other') {
			$donation_amount = sanitize_text_field( $form_post['donation-amount-other']  * 100 );
		} else {
			$donation_amount = sanitize_text_field( $form_post['donation-amount'] );
		}
		$personal_preferences = sanitize_text_field( serialize($form_post['personal-preferences']) );
		$gift_aid = sanitize_text_field( $form_post['gift_aid'][0] );
		$hear_about_us = sanitize_text_field( $form_post['hear-about-us'] );
		$hear_about_us_other = sanitize_text_field( $form_post['about-your-donation'] );

		$redirect_id = sanitize_text_field( $redirect_id );
		
		$wpdb->query( $wpdb->prepare( 
			"
				INSERT INTO $table_name
				( donation_type, donation_amount, donation_date, gc_redirect_identifier, personal_preferences, gift_aid, hear_about_us, hear_about_us_other )
				VALUES ( %s, %d, %s, %s, %s, %s, %s, %s )
			", 
		        array(
			    $donation_type,
			    $donation_amount,
				$timenow,
				$redirect_id,
				$personal_preferences,
				$gift_aid,
				$hear_about_us,
				$hear_about_us_other
			) 
		) );
		
		$donation_id = $wpdb->insert_id;
		
		$hft_id = $donation_id.date('my', strtotime('now'));
		
		$wpdb->query( $wpdb->prepare( 
			"
				UPDATE $table_name
				SET hft_id = $hft_id
				WHERE donation_id = $donation_id
			",
			array(
				$hft_id,$donation_id
			)
		) );
		
		return $wpdb->insert_id;
	}
	
	public function insert_donation($form_post, $donor_id, $stripe_id) {
		
		global $wpdb; // this is how you get access to the database
			
		$table_name = $wpdb->prefix . 'hft_donations';
			
		$timenow = date('Y-m-d H:i:s', strtotime('now'));
		
		$donation_type = sanitize_text_field( $form_post['donation-type'] );
		$donation_amount = sanitize_text_field( $form_post['donation-amount'] );
		$stripe_id = sanitize_text_field( $stripe_id );

		
		$wpdb->query( $wpdb->prepare( 
			"
				INSERT INTO $table_name
				( donation_type, donation_amount, donor_id, donation_date, stripe_identifier )
				VALUES ( %s, %d, %d, %s, %s )
			", 
		        array(
			    $donation_type,
			    $donation_amount,
			    $donor_id,
				$timenow,
				$stripe_id,
			) 
		) );
		
		return $wpdb->insert_id;

	}


	public function insert_donor($form_post) {
		
		global $wpdb; // this is how you get access to the database
			
		$table_name = $wpdb->prefix . 'hft_donors';
			
		$timenow = date('Y-m-d H:i:s', strtotime('now'));
		
		$billing_name = sanitize_text_field( $form_post['stripeBillingName'] );
		$donor_email = sanitize_text_field( $form_post['stripeEmail'] );
		
		$billing_address_line_1 = sanitize_text_field( $form_post['stripeBillingAddressLine1'] );
		$billing_zip = sanitize_text_field( $form_post['stripeBillingAddressZip'] );
		$billing_city = sanitize_text_field( $form_post['stripeBillingAddressCity'] );
		$billing_country = sanitize_text_field( $form_post['stripeBillingAddressCountry'] );
		$billing_country_code = sanitize_text_field( $form_post['stripeBillingAddressCountryCode'] );

		$hear_about_us = sanitize_text_field( $form_post['hear-about-us'] );
		$hear_about_us_other = sanitize_text_field( $form_post['about-your-donation'] );
		$personal_preferences = sanitize_text_field( serialize($form_post['personal-preferences']) );
		$gift_aid = sanitize_text_field( $form_post['gift_aid'][0] );
				
		
		
		$donor_exists = $wpdb->get_row(
			$wpdb->prepare(
				"
					SELECT * FROM $table_name
					WHERE donor_email = %s
				",
				array(
					$donor_email
				)
			)
		);
		
		if ($donor_exists) {
			
			$wpdb->query( $wpdb->prepare( 
				"
					UPDATE $table_name
					SET donor_name = '$billing_name', billing_address_line_1 = '$billing_address_line_1', billing_zip = '$billing_zip', billing_city = '$billing_city', billing_country = '$billing_country', billing_country_code = '$billing_country_code', hear_about_us = '$hear_about_us', hear_about_us_other = '$hear_about_us_other', personal_preferences = '$personal_preferences', gift_aid = '$gift_aid'
					WHERE donor_id = %s
				", 
			        array(
			        $donor_exists->donor_id
				) 
			) );

		
			return $donor_exists->donor_id;
			
		} else {
			
			$wpdb->query( $wpdb->prepare( 
				"
					INSERT INTO $table_name
					( donor_name, donor_email, billing_address_line_1, billing_zip, billing_city, billing_country, billing_country_code, created_date, hear_about_us, hear_about_us_other, personal_preferences, gift_aid)
					VALUES ( %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s )
				", 
			        array(
			        $billing_name,
				    $donor_email,
					$billing_address_line_1, 
					$billing_zip, 
					$billing_city,
					$billing_country,
					$billing_country_code,
					$timenow,
					$hear_about_us,
					$hear_about_us_other,
					$personal_preferences,
					$gift_aid
				) 
			) );
			
			return $wpdb->insert_id;
		}
		
	}
	
	public function donation_notification($form_post, $donor_id, $donation_id) {
		
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/partials/email-notifications.php';
		
	}
	
	public function get_donor_record($donor_id) {
		global $wpdb; // this is how you get access to the database
			
		$table_name = $wpdb->prefix . 'hft_donors';

		$results = $wpdb->get_row(
			$wpdb->prepare(
				"
					SELECT * FROM $table_name
					WHERE donor_id = %d
				",
				array(
					$donor_id
				)
			)
		);
		
		return $results;
		
	}

	public function get_donation_record($donation_id) {
		global $wpdb; // this is how you get access to the database
			
		$table_name = $wpdb->prefix . 'hft_donations';

		$results = $wpdb->get_row(
			$wpdb->prepare(
				"
					SELECT * FROM $table_name
					WHERE donation_id = %d
				",
				array(
					$donation_id
				)
			)
		);
		
		return $results;
		
	}
	
	public function get_users_email_preference($array) {
		
		$bool = true;
		//$array = unserialize($array);
		
		if (in_array('email-confirmation', $array)) {
			$bool = false;
		}
		return $bool;
	}
	
	public function get_users_all_preference($array) {
				
		$array = unserialize($array);
		$donation_choices = $this->get_donation_choices();
		$prefs = array();
		
		foreach($donation_choices as $k => $donation_choice){
			if (in_array('personal-preferences', $donation_choice)) {
				$options = $donation_choice['options'];
			}
		}
		
		foreach ($array as $k => $v) {
			$prefs[] = $options[$v];
		}
		
		$prefs = join('<br>', $prefs);
		
		return $prefs;
	}
	
	public function gc_current_plans() {

		$gc_current_plans = array(
			3 => 'https://pay.gocardless.com/AL0000N2JD4F1Z',
			5 => 'https://pay.gocardless.com/AL0000N2JKCYAE',
			10 => 'https://pay.gocardless.com/AL0000N2JMN8Z3',
		);
		
		return $gc_current_plans;
	}

	/**
	 * Retrive the access token for GoCardless.
	 *
	 * @since    2.0.0
	 * 
	 */
	public function hft_gocardless_access_token() {
		$options = get_option( 'hft_gocardless_settings_settings' );
		$access_token = $options['hft_gocardless_settings_text_field_0'];

		// temp elliott sandbox account uncomment to go live
		// $access_token = 'sandbox_cC61jvGE2-MzU-un9Yqvzyou6DJl-OewCiWhhwxy'; // squareone
		// $access_token = 'sandbox_t-gNLM_2FfBE6oPQqNUE4Yq-LEf3i4H3ss4f-_rR'; // hft sandbox
		
		return $access_token;
	}
}
