<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.squareonemd.co.uk
 * @since      2.0.0
 *
 * @package    Hopeft_Donate
 * @subpackage Hopeft_Donate/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
