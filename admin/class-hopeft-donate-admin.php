<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.squareonemd.co.uk
 * @since      2.0.0
 *
 * @package    Hopeft_Donate
 * @subpackage Hopeft_Donate/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Hopeft_Donate
 * @subpackage Hopeft_Donate/admin
 * @author     Your Name <email@example.com>
 */
class Hopeft_Donate_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    2.0.0
	 * @access   private
	 * @var      string    $hopeft_donate    The ID of this plugin.
	 */
	private $hopeft_donate;

	/**
	 * The version of this plugin.
	 *
	 * @since    2.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    2.0.0
	 * @param      string    $hopeft_donate       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $hopeft_donate, $version ) {

		$this->hopeft_donate = $hopeft_donate;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    2.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Hopeft_Donate_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Hopeft_Donate_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->hopeft_donate, plugin_dir_url( __FILE__ ) . 'css/hopeft-donate-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    2.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Hopeft_Donate_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Hopeft_Donate_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->hopeft_donate, plugin_dir_url( __FILE__ ) . 'js/hopeft-donate-admin.js', array( 'jquery' ), $this->version, false );

	}
	
	
	
	
	/**
	 * Creates a submenu item for the GoCardless api access token.
	 *
	 * @since    2.0.0
	 * 
	 */
	public function hft_gocardless_settings_add_admin_menu(  ) { 
	
		add_submenu_page( 'options-general.php', 'GoCardless', 'GoCardless', 'manage_options', 'gocardless', array($this, 'hft_gocardless_settings_options_page' ) );
	
	}

	/**
	 * Initialise the sections and settings for gocardless.
	 *
	 * @since    2.0.0
	 * 
	 */
	public function hft_gocardless_settings_settings_init(  ) { 
	
		register_setting( 'hftGoCardless', 'hft_gocardless_settings_settings' );
	
		add_settings_section(
			'hft_gocardless_settings_hftGoCardless_section', 
			__( 'API Settings', 'hft_textdomain' ), 
			array ($this, 'hft_gocardless_settings_settings_section_callback'), 
			'hftGoCardless'
		);
	
		add_settings_field( 
			'hft_gocardless_settings_text_field_0', 
			__( 'Access Token', 'hft_textdomain' ), 
			array ($this, 'hft_gocardless_settings_text_field_0_render'), 
			'hftGoCardless', 
			'hft_gocardless_settings_hftGoCardless_section' 
		);
	
	/*
		add_settings_field( 
			'hft_gocardless_settings_checkbox_field_1', 
			__( 'Settings field description', 'hft_textdomain' ), 
			'hft_gocardless_settings_checkbox_field_1_render', 
			'hftGoCardless', 
			'hft_gocardless_settings_hftGoCardless_section' 
		);
	*/
	
	
	}

	/**
	 * Callback to render a settings input.
	 *
	 * @since    2.0.0
	 * 
	 */
	public function hft_gocardless_settings_text_field_0_render(  ) { 
	
		$options = get_option( 'hft_gocardless_settings_settings' );
		?>
		<input type='text' name='hft_gocardless_settings_settings[hft_gocardless_settings_text_field_0]' value='<?php echo $options['hft_gocardless_settings_text_field_0']; ?>'>
		<?php
	
	}


	/**
	 * Callback to render a settings checkbox - currently disabled.
	 *
	 * @since    2.0.0
	 * 
	 */
	public function hft_gocardless_settings_checkbox_field_1_render(  ) { 
	
		$options = get_option( 'hft_gocardless_settings_settings' );
		
		?>
		<input type='checkbox' name='hft_gocardless_settings_settings[hft_gocardless_settings_checkbox_field_1]' <?php checked( $options['hft_gocardless_settings_checkbox_field_1'], 1 ); ?> value='1'>
		<?php
	
	}

	/**
	 * Callback to render a settings description.
	 *
	 * @since    2.0.0
	 * 
	 */
	public function hft_gocardless_settings_settings_section_callback(  ) { 
	
		echo __( 'Paste the Access Token that has been generated in the GoCardless account.', 'hft_textdomain' );
	
	}

	/**
	 * Creates the contents of the submenu settings page.
	 *
	 * @since    2.0.0
	 * 
	 */

	public function hft_gocardless_settings_options_page(  ) { 
	
		?>
		<form action='options.php' method='post'>
	
			<h2>GoCardless</h2>
	
			<?php
			settings_fields( 'hftGoCardless' );
			do_settings_sections( 'hftGoCardless' );
			submit_button();
			?>
	
		</form>
		<?php
	
	}
	

}
